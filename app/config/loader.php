<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */

$loader->registerNamespaces(array(
    'ApplicationApi' => $GLOBALS[ 'APPLICATION' ][ 'controllersDir' ] . "api/",
    'AdminModul' => $GLOBALS[ 'APPLICATION' ][ 'controllersDir' ] . "admin/",
    'UserModul' => $GLOBALS[ 'APPLICATION' ][ 'controllersDir' ] . "user/",
    'QuranModul' => $GLOBALS[ 'APPLICATION' ][ 'controllersDir' ] . "quran/",
    'FrontModul' => $GLOBALS[ 'APPLICATION' ][ 'controllersDir' ] . "front/"
));
$loader->registerDirs(array(
    $GLOBALS[ 'APPLICATION' ][ 'controllersDir' ],
    $GLOBALS[ 'APPLICATION' ][ 'modelsDir' ],
    $GLOBALS[ 'APPLICATION' ][ 'libraryDir' ],
    $GLOBALS[ 'APPLICATION' ][ 'servicesDir' ],
    $GLOBALS[ 'APPLICATION' ][ 'objectsDir' ]
));
/*
$loader->registerClasses(
    array(
        "I18nLibrary"         => $config->application->libraryDir."/I18nLibrary.php",
        "T"         => $config->application->libraryDir."/T.php"
    )
);
*/
$loader->register();

//$test = I18nLibrary::getPhrase("app.title");