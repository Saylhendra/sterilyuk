<?php

class IndexController extends \ControllerBase{

    public function indexAction(){
        $this->response->redirect( base_url("/admin/login_page/index") );
    }

    public function removeAction(){
        $response = new \ResponseObject();
        $this->db->begin();
        $id = $this->request->getPost("id");
        try {
            $data = \StrAccount::findFirstById($id);
            $data->delete();
            $response->message = \T::message("all.label.message.success_delete");
            $this->db->commit();
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
    }

}