<?php

class LoginController extends ControllerBase{

	public function initialize()
	{
		$this->init();
	}

	public function indexAction()
	{
		$message = isset($_GET['message'])?$_GET['message']:"";
		$model = array();
		$model['message'] = $message;
        $model['mode'] = "User";
        $model['action'] = base_url()."/login/authAdmin";
        $this->view->partial('login/login_general',$model);
	}

    public function quranAction()
    {
        $this->response->redirect(base_url('quran/dashboard'));
    }

    public function adminAction(){
        $message = isset($_GET['message'])?$_GET['message']:"";
        $model = array();
        $model['message'] = $message;
        $model['mode'] = "Admin";
        $model['action'] = base_url()."/login/authAdmin";

        $model['success_url'] = "admin/dashboard";

        $this->view->partial('admin/login_page/index', $model);
    }

    /*================================
    This is important for Login
    ==================================*/
    public function authAdminAction(){
        $result = new ResponseObject();
        if ( $this->request->isPost() ) {
            try{
                $username = $this->request->getPost("username");
                $password = $this->request->getPost("password");

                if( $username == 'sup3r' && $password == '123'){
                    $this->isSuccess = TRUE;
                    $this->session->set("session_username", "super");
                    $this->session->set("isLoginAdmin", TRUE);
                }else{
                    $result = MemberService::signinMemberApp($username, $password, "", "", "");
                    if ( $result->isSuccess ) {
                        $user = $result->data;
                        $this->isSuccess = TRUE;
                        $this->session->set("session_username", $user['username']);
                        $this->session->set("isLoginAdmin", TRUE);
                        /*$this->session->set("name_admin", $user->nama);
                        $this->session->set("id_admin", $user->id);
                        $this->session->set("isAdmin", $user->is_admin);
                        $this->session->set("id_division", $user->id_division);*/
                        /*$dataCustomer = \StrAccount::findFirstByIdUser($user->email);
                        if( !empty($dataCustomer ) ){
                            $this->session->set("idCustomer", $user->SqCustomer->id);
                            $this->session->set("objCustomer", $dataCustomer);
                            $this->session->set("id_customer", $dataCustomer->id);
                            $this->session->set("is_mr", $dataCustomer->is_membina);
                        }*/
                    }
                }

            }catch (Exception $ex){
                $result->isSuccess = FALSE;
                $result->message = $ex->getTraceAsString();
            }
        }
        echo json_encode($result);
    }

	public function signupAction(){
        $response = new \ResponseObject();
        $this->db->begin();

        try {
            $email = $_POST['email'];
            $isDataExist = \PgAdmin::findFirstByEmail($email);

            if(!$isDataExist){
                $data = new \PgAdmin();
                $data->assign($_POST);
                $data->username = $data->email;
                $data->password = md5($data->password);
                $data->id = $this->uuidString();
                //$data->save();

                //$saveCustomer = LoginController::doCreateCustomer($data);
                $hasil = \EmailService::sendEmailRegistration($email, $data->username, "0813", $data->password);
                LoggerLibrary::logDebug("Pasca eksekusi email services".$hasil);

                $response->isSuccess = TRUE;
                $response->message = \T::message("all.label.message.success_save");
                $response->data = $data;
                $this->db->commit();
            }else{
                $response->isSuccess = FALSE;
                $response->message = "Email sudah ada.";
            }
        }catch (\Exception $ex){
            $this->db->rollback();
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        echo json_encode($response);
	}

    public function doCreateCustomer($dataAdmin){
        $response = new \ResponseObject();
        try {
            $customer = \SqCustomer::findFirst(array(
                "id=:aidi: or email=:email:",
                "bind" => array(
                    "aidi"=>$dataAdmin->id_customer,
                    "email"=>$dataAdmin->email
                )
            ));

            if(!$customer){
                $data = new \SqCustomer();

                $data->id = $this->uuidString();

                $data->first_name = $dataAdmin->nama;
                $data->email = $dataAdmin->email;

                $data->path_small = $dataAdmin->path_small;
                $data->path_medium = $dataAdmin->path_medium;
                $data->path_large = $dataAdmin->path_large;
                $data->path_thumbnails = $dataAdmin->path_thumbnails;

                $data->tipe = \TypeCustomer::$GUEST;

                $data->created_date = date('Y-m-d H:i:s');
                $data->update_date = date('Y-m-d H:i:s');
                $data->save();
            }
        }catch (\Exception $ex){
            $response->isSuccess = false;
            $response->message = $ex->getMessage();
        }
        return $response;
    }

    public function outUserAction()
    {
        $this->cookies->set('remember', FALSE, time() + 15 * 86400);
        $this->session->destroy();
        $this->response->redirect('/login');
    }

	public function logOutAdminAction()
	{
		$this->cookies->set('remember', FALSE, time() + 15 * 86400);
		$this->session->destroy();
		$this->response->redirect('/login/admin');
	}

    public function forgotAction(){

    }

}?>