<?php

class SignUpController extends \ControllerBase{

    private $TITLE = "Sign Up";
    private $URL = "sign_up";

    public function indexAction(){
        $model = array();
        $model['title'] = $this->TITLE;
        $model['url'] = $this->URL;
        $model['action'] = base_url()."/sign_up/doSignUp";
        $this->view->partial("sign_up/signup", $model);
    }

    public function doSignUpAction(){
        $response = MemberService::signUpMember($_POST['username'], $_POST['password']);
//        LoggerLibrary::logDebug("response: doSignUpAction");
//        LoggerLibrary::logDebug($response);
        return json_encode($response);
    }
}