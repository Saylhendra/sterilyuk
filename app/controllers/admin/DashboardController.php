<?php
namespace AdminModul;
use Phalcon\Http\Client\Exception;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class DashboardController extends AdminControllerBase{

    private $TITLE = "Steril Yuk Panel";
    private $TITLE_BREAD = "Dashboard";
    private $URL = "dashboard";

    public function indexAction(){

        $model = array();
        $model['title'] = $this->TITLE;
        $model['title_bread'] = $this->TITLE_BREAD;
        $model['url'] = $this->URL;
        //$this->view->partial('admin/' . $this->URL . '/index', $model);
        DashboardController::loadData($this->URL, $model);
    }

    public function loadData($url, $model){
        $dataArray = \JadwalService::listJadwal();
        $this->view->partial('admin/' . $url . '/index',
            array("model"=>$model, "listFromController"=>json_decode(json_encode($dataArray->data)))
        );
    }
}