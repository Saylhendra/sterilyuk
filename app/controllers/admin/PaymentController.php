<?php
namespace AdminModul;
use Phalcon\Http\Client\Exception;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class PaymentController extends AdminControllerBase{

    private $TITLE = "Payment";
    private $TITLE_BREAD = "Dashboard";
    private $URL = "payment";

    public function indexAction(){

        $model = array();
        $model['title'] = $this->TITLE;
        $model['title_bread'] = $this->TITLE_BREAD;
        $model['url'] = $this->URL;
        PaymentController::loadData($this->URL, $model);
    }

    public function loadData($url, $model){
        $dataArray = \StrPayment::find();
        $this->view->partial('admin/' . $url . '/index',
            array("model"=>$model, "listFromController"=>json_decode(json_encode($dataArray->toArray())))
        );
    }
}