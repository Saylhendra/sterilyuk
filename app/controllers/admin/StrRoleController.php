<?php
namespace AdminModul;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class StrRoleController extends \ControllerBase
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for str_role
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "StrRole", $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id";

        $str_role = StrRole::find($parameters);
        if (count($str_role) == 0) {
            $this->flash->notice("The search did not find any str_role");

            return $this->dispatcher->forward(array(
                "controller" => "str_role",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $str_role,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displayes the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a str_role
     *
     * @param string $id
     */
    public function editAction($id)
    {

        if (!$this->request->isPost()) {

            $str_role = StrRole::findFirstByid($id);
            if (!$str_role) {
                $this->flash->error("str_role was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "str_role",
                    "action" => "index"
                ));
            }

            $this->view->id = $str_role->id;

            $this->tag->setDefault("id", $str_role->id);
            $this->tag->setDefault("nama", $str_role->nama);
            $this->tag->setDefault("kode_role", $str_role->kode_role);
            $this->tag->setDefault("date_created", $str_role->date_created);
            $this->tag->setDefault("date_updated", $str_role->date_updated);
            
        }
    }

    /**
     * Creates a new str_role
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "str_role",
                "action" => "index"
            ));
        }

        $str_role = new StrRole();

        $str_role->id = $this->request->getPost("id");
        $str_role->nama = $this->request->getPost("nama");
        $str_role->kode_role = $this->request->getPost("kode_role");
        $str_role->date_created = $this->request->getPost("date_created");
        $str_role->date_updated = $this->request->getPost("date_updated");
        

        if (!$str_role->save()) {
            foreach ($str_role->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_role",
                "action" => "new"
            ));
        }

        $this->flash->success("str_role was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_role",
            "action" => "index"
        ));

    }

    /**
     * Saves a str_role edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "str_role",
                "action" => "index"
            ));
        }

        $id = $this->request->getPost("id");

        $str_role = StrRole::findFirstByid($id);
        if (!$str_role) {
            $this->flash->error("str_role does not exist " . $id);

            return $this->dispatcher->forward(array(
                "controller" => "str_role",
                "action" => "index"
            ));
        }

        $str_role->id = $this->request->getPost("id");
        $str_role->nama = $this->request->getPost("nama");
        $str_role->kode_role = $this->request->getPost("kode_role");
        $str_role->date_created = $this->request->getPost("date_created");
        $str_role->date_updated = $this->request->getPost("date_updated");
        

        if (!$str_role->save()) {

            foreach ($str_role->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_role",
                "action" => "edit",
                "params" => array($str_role->id)
            ));
        }

        $this->flash->success("str_role was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_role",
            "action" => "index"
        ));

    }

    /**
     * Deletes a str_role
     *
     * @param string $id
     */
    public function deleteAction($id)
    {

        $str_role = StrRole::findFirstByid($id);
        if (!$str_role) {
            $this->flash->error("str_role was not found");

            return $this->dispatcher->forward(array(
                "controller" => "str_role",
                "action" => "index"
            ));
        }

        if (!$str_role->delete()) {

            foreach ($str_role->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "str_role",
                "action" => "search"
            ));
        }

        $this->flash->success("str_role was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "str_role",
            "action" => "index"
        ));
    }

}
