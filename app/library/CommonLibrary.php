<?php
use Phalcon\Logger\Adapter\File as FileAdapter;

class CommonLibrary
{
	private static $_modelsManager;
	private static $_db;

	public static function getModelsManager(){
		if(!isset(self::$_modelsManager)){
			$di = Phalcon\DI::getDefault();
			self::$_modelsManager = $di["modelsManager"];
		}
		return self::$_modelsManager;
	}

	public static function getRequestGeoInfo()
	{
		$ip_address = $_SERVER[ 'REMOTE_ADDR' ];
		if ( ip2long($ip_address) == -1 || ip2long($ip_address) === FALSE ) {
			$ip_address = "";
		}
		$url = "http://api.hostip.info/get_json.php?ip=" . $ip_address;
		$result = file_get_contents($url);
		$result = json_decode($result, 1);

		//{"country_name":"INDONESIA","country_code":"ID","city":"Jakarta","ip":"118.137.221.184"}
		return $result;
	}

	public static function generateNoreg($postData){
		$result = 'REG'.substr($postData['id_acc'], -3)."/".substr($postData['id_jadwal'], -3);
		return $result;
	}

	public static function getSubString($parString, $number){
        //$result = "string: out of bond";
        //$clearWhiteSpace = explode(" ", $parString);
        $result = substr($parString, $number);
        return $result;
    }
}