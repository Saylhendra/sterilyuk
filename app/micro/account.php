<?php
$app = getMicroApp();
global $request;
$request = getRequestApp();

/**
 * Display `OK` message
 *
 * @return json string `OK`
 */
$app->get('/api/account', function () {
    header('HTTP/1.0 200 OK');
    header('Content-Type: application/json');
    $data = array("message" => T::message("api.message.info.httpok"));

    echo json_encode($data);
});

/* START POST API(s)
======================================================== */
$app->post('/api/account/signup', function () {

    global $request;

    $username = $request->getPost('username');
    $password = $request->getPost('password');
    $device_id = $request->getPost('device_id');
    $os_type = $request->getPost('os_type');
    $app_key = $request->getPost('app_key');

    $result = \MemberService::signUpMember($username, $password, $device_id, $os_type, $app_key);

    header('Content-Type: application/json');
    if ($result->isSuccess === FALSE) {
        header('HTTP/1.0 401 Authorization Required');
    }

    echo json_encode($result);
});
/* END POST API(s)
======================================================== */