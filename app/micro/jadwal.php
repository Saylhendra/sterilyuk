<?php
$app = getMicroApp();
global $request;
$request = getRequestApp();

/**
 * Display `OK` message
 *
 * @return json string `OK`
 */

$app->get('/api/jadwal', function () {
    header('HTTP/1.0 200 OK');
    header('Content-Type: application/json');
    $data = array("message" => T::message("api.message.info.httpok"));
    echo json_encode($data);
});

/* START GET API(s)
======================================================== */
//LISTING: JADWAL
$app->get('/api/jadwal/list', function () {
    global $request;
    header('Content-Type: application/json');

    $app_key = $request->getQuery('app_key');
    $session_key = $request->getQuery('session_key');

    //$idMember = MemberService::getUserLoggedOnBySessionToken($session_key);
    if (AppsService::checkAppKey($app_key) === FALSE) {
        header('HTTP/1.0 401 Authorization Required');
        $newsDataHeader = array(
            "isSuccess" => FALSE,
            "message" => EnumAppKey::getString(false) );
        echo json_encode($newsDataHeader);
        die();
    }
    /*if (MemberService::checkSessionToken($session_key) === FALSE) {
        header('HTTP/1.0 401 Authorization Required');
        $businessDataHeader = array(
            "isSuccess" => FALSE,
            "message" => T::message("login.message.invalidsessionkey"));
        echo json_encode($businessDataHeader);
        die();
    }*/

    //$request = JadwalService::listJadwal();
    $request = JadwalService::listJadwalAndDetail();

    echo json_encode($request);
});
//LISTING: JADWAL DETIL
$app->get('/api/jadwal/jadwal_detil_list', function () {
    global $request;
    header('Content-Type: application/json');

    $app_key = $request->getQuery('app_key');
    $session_key = $request->getQuery('session_key');

    //$idMember = MemberService::getUserLoggedOnBySessionToken($session_key);
    if (AppsService::checkAppKey($app_key) === FALSE) {
        header('HTTP/1.0 401 Authorization Required');
        $newsDataHeader = array(
            "isSuccess" => FALSE,
            "message" => EnumAppKey::getString(false) );
        echo json_encode($newsDataHeader);
        die();
    }
    /*if (MemberService::checkSessionToken($session_key) === FALSE) {
        header('HTTP/1.0 401 Authorization Required');
        $businessDataHeader = array(
            "isSuccess" => FALSE,
            "message" => T::message("login.message.invalidsessionkey"));
        echo json_encode($businessDataHeader);
        die();
    }*/

    $request = JadwalService::listJadwalDetil();

    echo json_encode($request);
});
/* END GET API(s)
======================================================== */

/* START POST API(s)
======================================================== */
//NEW: JADWAL
$app->post('/api/jadwal/new', function () {
    global $request;
    $app_key = $request->getPost('app_key');
    $session_key = $request->getPost('session_key');

    $parIdJadwalTipe = $request->getPost('parIdJadwalTipe');
    $parLokasi = $request->getPost('parLokasi');
    $parAlamat = $request->getPost('parAlamat');
    $parTglLaksana = $request->getPost('parTglLaksana');
    $parNotes = $request->getPost('parNotes');

    if (AppsService::checkAppKey($app_key) === FALSE) {
        header('HTTP/1.0 401 Authorization Required');
        $newsDataHeader = array(
            "isSuccess" => FALSE,
            "message" => EnumAppKey::getString(false) );
        echo json_encode($newsDataHeader);
        die();
    }

    $dataPost = array(
        "id_jadwal_tipe" => $parIdJadwalTipe,
        "lokasi" => $parLokasi,
        "alamat" => $parAlamat,
        "tgl_laksana" => $parTglLaksana,
        "notes" => $parNotes,
        "str_tgl" => "",
        "str_bulan" => "",
        "str_thn" => "",
        "sts_aktif" => ""
    );
    $result = \JadwalService::saveJadwal($dataPost);

    echo json_encode($result);
});
//NEW: JADWAL DETIL
$app->post('/api/jadwal/jadwal_detil_new', function () {
    global $request;
    $app_key = $request->getPost('app_key');
    $session_key = $request->getPost('session_key');

    $parIdJadwal = $request->getPost('parIdJadwal');
    $parIdJadwalShift = $request->getPost('parIdJadwalShift');

    $parBetinaKuota = $request->getPost('parBetinaKuota');
    $parBetinaSisaKuota = $request->getPost('parBetinaSisaKuota');
    $parJantanKuota = $request->getPost('parJantanKuota');
    $parJantanSisaKuota = $request->getPost('parJantanSisaKuota');

    if (AppsService::checkAppKey($app_key) === FALSE) {
        header('HTTP/1.0 401 Authorization Required');
        $newsDataHeader = array(
            "isSuccess" => FALSE,
            "message" => EnumAppKey::getString(false) );
        echo json_encode($newsDataHeader);
        die();
    }

    $dataPost = array(
        "betina_kuota" => $parBetinaKuota,
        "betina_sisa_kuota" => $parBetinaSisaKuota,
        "jantan_kuota" => $parJantanKuota,
        "jantan_sisa_kuota" => $parJantanSisaKuota,
        "tot_kuota" => "",
        "tot_sisa" => ""
    );
    $result = \JadwalService::saveJadwalDetil($dataPost, $parIdJadwal, $parIdJadwalShift);

    echo json_encode($result);
});
/*-/////////////////////////////////////////////////////-*/
//DELETING: JADWAL
$app->post('/api/jadwal/delete', function () {
    global $request;
    $app_key = $request->getPost('app_key');
    $session_key = $request->getPost('session_key');

    $parIdJadwal = $request->getPost('parIdJadwal');

    if (AppsService::checkAppKey($app_key) === FALSE) {
        header('HTTP/1.0 401 Authorization Required');
        $newsDataHeader = array(
            "isSuccess" => FALSE,
            "message" => EnumAppKey::getString(false) );
        echo json_encode($newsDataHeader);
        die();
    }

    $result = \JadwalService::deleteJadwal($parIdJadwal);

    echo json_encode($result);
});
//DELETING: JADWAL DETIL
$app->post('/api/jadwal/jadwal_detil_delete', function () {
    global $request;
    $app_key = $request->getPost('app_key');
    $session_key = $request->getPost('session_key');

    $parIdJadwal = $request->getPost('parIdJadwal');
    $parIdJadwalShift = $request->getPost('parIdJadwalShift');

    if (AppsService::checkAppKey($app_key) === FALSE) {
        header('HTTP/1.0 401 Authorization Required');
        $newsDataHeader = array(
            "isSuccess" => FALSE,
            "message" => EnumAppKey::getString(false) );
        echo json_encode($newsDataHeader);
        die();
    }

    $result = \JadwalService::deleteJadwalDetil($parIdJadwal, $parIdJadwalShift);

    echo json_encode($result);
});
/* END POST API(s)
======================================================== */