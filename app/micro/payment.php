<?php
$app = getMicroApp();
global $request;
$request = getRequestApp();

/**
 * Display `OK` message
 *
 * @return json string `OK`
 */

$app->get('/api/payment', function () {
    header('HTTP/1.0 200 OK');
    header('Content-Type: application/json');
    $data = array("message" => T::message("api.message.info.httpok"));
    echo json_encode($data);
});

/* START GET API(s)
======================================================== */
//LISTING
$app->get('/api/payment/list', function () {
    global $request;
    header('Content-Type: application/json');

    $app_key = $request->getQuery('app_key');
    $session_key = $request->getQuery('session_key');

    //$idMember = MemberService::getUserLoggedOnBySessionToken($session_key);
    if (AppsService::checkAppKey($app_key) === FALSE) {
        header('HTTP/1.0 401 Authorization Required');
        $newsDataHeader = array(
            "isSuccess" => FALSE,
            "message" => EnumAppKey::getString(false) );
        echo json_encode($newsDataHeader);
        die();
    }
    /*if (MemberService::checkSessionToken($session_key) === FALSE) {
        header('HTTP/1.0 401 Authorization Required');
        $businessDataHeader = array(
            "isSuccess" => FALSE,
            "message" => T::message("login.message.invalidsessionkey"));
        echo json_encode($businessDataHeader);
        die();
    }*/

    $request = RegistrasiService::listRegistrasi();

    echo json_encode($request);
});
/* END GET API(s)
======================================================== */

/* START POST API(s)
======================================================== */
//NEW
$app->post('/api/payment/new', function () {
    global $request;
    $app_key = $request->getPost('app_key');
    $session_key = $request->getPost('session_key');

    $parNoReg = $request->getPost('parNoReg');
    $buktiBayarImg = $request->getPost('parBuktiPembayaranImage');

    if (AppsService::checkAppKey($app_key) === FALSE) {
        header('HTTP/1.0 401 Authorization Required');
        $newsDataHeader = array(
            "isSuccess" => FALSE,
            "message" => EnumAppKey::getString(false) );
        echo json_encode($newsDataHeader);
        die();
    }

    $dataPost = array(
        "no_reg" => $parNoReg
    );
    $result = \PaymentService::savePayment($dataPost, $parNoReg, $buktiBayarImg);

    echo json_encode($result);
});
//APPROVING
$app->post('/api/payment/konfirmasi', function () {
    global $request;
    $app_key = $request->getPost('app_key');
    $session_key = $request->getPost('session_key');


    $parNoreg = $request->getPost('parNoreg');
    $parNotes = $request->getPost('parNotes');

    if (AppsService::checkAppKey($app_key) === FALSE) {
        header('HTTP/1.0 401 Authorization Required');
        $newsDataHeader = array(
            "isSuccess" => FALSE,
            "message" => EnumAppKey::getString(false) );
        echo json_encode($newsDataHeader);
        die();
    }

    $dataPost = array(
        "no_reg" => $parNoreg,
        "notes" => $parNotes
    );
    $result = \RegistrasiService::approveRegistrasi($dataPost, $parNoreg);

    echo json_encode($result);
});

/*-/////////////////////////////////////////////////////-*/
//DELETING
$app->post('/api/payment/delete', function () {
    global $request;
    $app_key = $request->getPost('app_key');
    $session_key = $request->getPost('session_key');

    $parNoreg = $request->getPost('parNoreg');

    if (AppsService::checkAppKey($app_key) === FALSE) {
        header('HTTP/1.0 401 Authorization Required');
        $newsDataHeader = array(
            "isSuccess" => FALSE,
            "message" => EnumAppKey::getString(false) );
        echo json_encode($newsDataHeader);
        die();
    }

    $result = \RegistrasiService::deleteRegistrasi($parNoreg);

    echo json_encode($result);
});
/* END POST API(s)
======================================================== */