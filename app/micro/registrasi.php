<?php
$app = getMicroApp();
global $request;
$request = getRequestApp();

/**
 * Display `OK` message
 *
 * @return json string `OK`
 */

$app->get('/api/registrasi', function () {
    header('HTTP/1.0 200 OK');
    header('Content-Type: application/json');
    $data = array("message" => T::message("api.message.info.httpok"));
    echo json_encode($data);
});

/* START GET API(s)
======================================================== */
//LISTING
$app->get('/api/registrasi/list', function () {
    global $request;
    header('Content-Type: application/json');

    $app_key = $request->getQuery('app_key');
    $session_key = $request->getQuery('session_key');

    //$idMember = MemberService::getUserLoggedOnBySessionToken($session_key);
    if (AppsService::checkAppKey($app_key) === FALSE) {
        header('HTTP/1.0 401 Authorization Required');
        $newsDataHeader = array(
            "isSuccess" => FALSE,
            "message" => EnumAppKey::getString(false) );
        echo json_encode($newsDataHeader);
        die();
    }
    /*if (MemberService::checkSessionToken($session_key) === FALSE) {
        header('HTTP/1.0 401 Authorization Required');
        $businessDataHeader = array(
            "isSuccess" => FALSE,
            "message" => T::message("login.message.invalidsessionkey"));
        echo json_encode($businessDataHeader);
        die();
    }*/

    $request = RegistrasiService::listRegistrasi();

    echo json_encode($request);
});
/* END GET API(s)
======================================================== */

/* START POST API(s)
======================================================== */
//NEW
$app->post('/api/registrasi/new', function () {
    global $request;
    $app_key = $request->getPost('app_key');
    $session_key = $request->getPost('session_key');

    $parIdAcc = $request->getPost('parIdAcc');
    $parIdJadwal = $request->getPost('parIdJadwal');
    $parNotes = $request->getPost('parNotes');

    if (AppsService::checkAppKey($app_key) === FALSE) {
        header('HTTP/1.0 401 Authorization Required');
        $newsDataHeader = array(
            "isSuccess" => FALSE,
            "message" => EnumAppKey::getString(false) );
        echo json_encode($newsDataHeader);
        die();
    }

    $dataPost = array(
        "id_acc" => $parIdAcc,
        "id_jadwal" => $parIdJadwal,
        "notes" => $parNotes
    );
    $result = \RegistrasiService::saveRegistrasi($dataPost);

    echo json_encode($result);
});
//APPROVING
$app->post('/api/registrasi/approve', function () {
    global $request;
    $app_key = $request->getPost('app_key');
    $session_key = $request->getPost('session_key');


    $parNoreg = $request->getPost('parNoreg');
    $parNotes = $request->getPost('parNotes');

    if (AppsService::checkAppKey($app_key) === FALSE) {
        header('HTTP/1.0 401 Authorization Required');
        $newsDataHeader = array(
            "isSuccess" => FALSE,
            "message" => EnumAppKey::getString(false) );
        echo json_encode($newsDataHeader);
        die();
    }

    $dataPost = array(
        "no_reg" => $parNoreg,
        "notes" => $parNotes
    );
    $result = \RegistrasiService::approveRegistrasi($dataPost, $parNoreg);

    echo json_encode($result);
});

/*-/////////////////////////////////////////////////////-*/
//DELETING
$app->post('/api/registrasi/delete', function () {
    global $request;
    $app_key = $request->getPost('app_key');
    $session_key = $request->getPost('session_key');

    $parNoreg = $request->getPost('parNoreg');

    if (AppsService::checkAppKey($app_key) === FALSE) {
        header('HTTP/1.0 401 Authorization Required');
        $newsDataHeader = array(
            "isSuccess" => FALSE,
            "message" => EnumAppKey::getString(false) );
        echo json_encode($newsDataHeader);
        die();
    }

    $result = \RegistrasiService::deleteRegistrasi($parNoreg);

    echo json_encode($result);
});
/* END POST API(s)
======================================================== */