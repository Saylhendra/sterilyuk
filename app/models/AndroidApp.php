<?php

class AndroidApp extends \Phalcon\Mvc\Model
{
    public $id;
    public $app_name;
    public $app_author;
    public $app_key;

    public function getSource()
    {
        return 'android_app';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return AndroidApp[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return AndroidApp
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
