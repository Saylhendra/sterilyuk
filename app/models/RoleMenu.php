<?php

class RoleMenu extends \Phalcon\Mvc\Model
{

    public $id_role;
    public $role_name;
    public $id_menu;
    public $menu_name;
    public $date_created;
    public $date_updated;

    public function initialize()
    {
        $this->belongsTo('id_role', 'Role', 'id', array("alias"=>"TblRole"));
        $this->belongsTo('id_menu', 'Menu', 'id', array("alias"=>"TblMenu"));
    }

    public function getSource()
    {
        return 'role_menu';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RoleMenu[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RoleMenu
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
