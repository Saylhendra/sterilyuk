<?php

class SessionToken extends \Phalcon\Mvc\Model
{
    public $id;
    public $id_app;
    public $id_account;
    public $session_key;
    public $device_id;
    public $os_type;
    public $date_created;
    public $session_log;

    public function initialize(){
        $this->belongsTo('id_account', 'UserAccount', 'id', array('alias' => 'TblUserAccount'));
        $this->belongsTo('id_app', 'AndroidApp', 'id', array('alias' => 'TblAndroidApp'));
    }

    public function getSource()
    {
        return 'session_token';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SessionToken[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SessionToken
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
