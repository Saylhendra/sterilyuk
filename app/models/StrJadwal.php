<?php

class StrJadwal extends \Phalcon\Mvc\Model
{
    public $id;
    public $id_jadwal_tipe;

    public $lokasi;
    public $alamat;

    public $tgl_laksana;
    public $str_tgl;
    public $str_bulan;
    public $str_thn;
    public $sts_aktif;

    public $bts_tempo_bayar;

    public $notes;

    public $date_created;
    public $date_update;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'id_jadwal_tipe' => 'id_jadwal_tipe', 
            'lokasi' => 'lokasi', 
            'alamat' => 'alamat', 
            'tgl_laksana' => 'tgl_laksana', 
            'str_tgl' => 'str_tgl', 
            'str_bulan' => 'str_bulan', 
            'str_thn' => 'str_thn', 
            'sts_aktif' => 'sts_aktif',

            'bts_tempo_bayar' => 'bts_tempo_bayar',
            'notes' => 'notes',

            'date_created' => 'date_created', 
            'date_update' => 'date_update'
        );
    }

}
