<?php

class StrJadwalDetil extends \Phalcon\Mvc\Model
{

    public $id;
    public $id_jadwal;
    public $id_jadwal_shift;

    public $betina_kuota;
    public $betina_sisa_kuota;
    public $jantan_kuota;
    public $jantan_sisa_kuota;
    public $tot_kuota;
    public $tot_sisa;

    public $date_created;
    public $date_update;

    public function initialize()
    {
        $this->belongsTo('id_jadwal', 'StrJadwal', 'id', array("alias"=>"TblStrJadwal"));
        $this->belongsTo('id_jadwal_shift', 'StrJadwalShift', 'id', array("alias"=>"TblStrJadwalShift"));
    }

    public function getSource()
    {
        return 'str_jadwal_detil';
    }

    /**
     * Independent Column Mapping.
     */
//    public function columnMap()
//    {
//        return array(
//            'id' => 'id',
//            'id_jadwal' => 'id_jadwal',
//            'id_jadwal_shift' => 'id_jadwal_shift',
//            'betina_kuota' => 'betina_kuota',
//            'betina_sisa_kuota' => 'betina_sisa_kuota',
//            'jantan_kuota' => 'jantan_kuota',
//            'jantan_sisa_kuota' => 'jantan_sisa_kuota',
//            'tot_kuota' => 'tot_kuota',
//            'tot_sisa' => 'tot_sisa',
//            'date_created' => 'date_created',
//            'date_update' => 'date_update'
//        );
//    }

}
