<?php

class StrPayment extends \Phalcon\Mvc\Model
{
    public $id;
    public $id_registrasi;
    public $no_reg;
    public $date_created;
    public $date_update;

    public function initialize()
    {
        $this->belongsTo('id_registrasi', 'StrRegistrasi', 'id', array("alias"=>"TblStrRegistrasi"));
    }

    public function getSource()
    {
        return 'str_payment';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return StrPayment[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return StrPayment
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
