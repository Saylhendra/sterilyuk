<?php

class StrPaymentDetil extends \Phalcon\Mvc\Model
{
    public $id;
    public $id_payment;

    public $path_original;
    public $path_large;
    public $path_medium;
    public $path_small;
    public $path_thumbnails;

    public $tgl_transfer;
    public $nama_pengirim;
    public $norek_transfer;
    public $jumlah_transfer;

    public $date_created;
    public $date_update;

    public function initialize()
    {
        $this->belongsTo('id_payment', 'StrPayment', 'id', array("alias"=>"TblStrPayment"));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'str_payment_detil';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return StrPaymentDetil[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return StrPaymentDetil
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
