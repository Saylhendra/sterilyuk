<?php

class StrRegistrasi extends \Phalcon\Mvc\Model
{
    public $id;
    public $id_acc;
    public $id_jadwal;

    public $no_reg;
    public $qrcode;

    public $sts_bayar;
    public $sts_aktif;

    public $notes;

    public $date_created;
    public $date_update;

    public function initialize()
    {
        $this->belongsTo('id_acc', 'UserAccount', 'id', array("alias"=>"TblUserAccount"));
        $this->belongsTo('id_jadwal', 'StrJadwal', 'id', array("alias"=>"TblStrJadwal"));
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'id_acc' => 'id_acc', 
            'id_jadwal' => 'id_jadwal', 
            'no_reg' => 'no_reg', 
            'qrcode' => 'qrcode', 
            'sts_bayar' => 'sts_bayar', 
            'sts_aktif' => 'sts_aktif', 
            'notes' => 'notes',
            'date_created' => 'date_created',
            'date_update' => 'date_update'
        );
    }

}
