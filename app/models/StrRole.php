<?php

class StrRole extends \Phalcon\Mvc\Model
{

    public $id;
    public $nama;
    public $kode_role;
    public $date_created;
    public $date_updated;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'nama' => 'nama', 
            'kode_role' => 'kode_role', 
            'date_created' => 'date_created', 
            'date_updated' => 'date_updated'
        );
    }

}
