<?php

class UserAccount extends \Phalcon\Mvc\Model
{

    public $id;
    public $username;
    public $password;
    public $id_role;
    public $date_created;
    public $date_updated;

    public function initialize()
    {
        $this->belongsTo('id_role', 'Role', 'id', array("alias"=>"TblRole"));
    }

    public function getSource()
    {
        return 'user_account';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserAccount[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserAccount
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
