<?php

use Phalcon\Mvc\Model\Validator\Email as Email;

class UserProfile extends \Phalcon\Mvc\Model
{

    public $id;
    public $id_account;
    public $full_name;
    public $address;
    public $phone;
    public $email;
    public $path_original;
    public $path_large;
    public $path_medium;
    public $path_small;
    public $path_thumbnails;
    public $date_created;
    public $date_updated;

    public function initialize()
    {
        $this->belongsTo('id_account', 'UserAccount', 'id', array("alias"=>"TblUserAccount"));
    }

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation()
    {
        $this->validate(
            new Email(
                array(
                    'field'    => 'email',
                    'required' => true,
                )
            )
        );

        if ($this->validationHasFailed() == true) {
            return false;
        }

        return true;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'user_profile';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserProfile[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserProfile
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
