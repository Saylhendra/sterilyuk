<?php
/**
 * Created by PhpStorm.
 * User: bitsolution
 * Date: 4/9/2015
 * Time: 4:41 PM
 */

class EnumAppKey{

	//column type news
    public static $FAILED = false;
    public static $SUCCESS = true;

    public static function getString($type){
        if( $type == self::$SUCCESS ){
            return "SUCCESSFULLY";
        }else{
            return "SORRY, WRONG APP KEY";
        }
    }
}