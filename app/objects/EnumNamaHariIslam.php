<?php
/**
 * Created by PhpStorm.
 * User: bitsolution
 * Date: 4/9/2015
 * Time: 4:41 PM
 */

class EnumNamaHariIslam
{
	//column type news
    public static $AHAD = 1;
    public static $ISNAYNI = 2;
    public static $TSULAATSA = 3;
    public static $ARBIA = 4;
    public static $KHAMIS = 5;
    public static $JUMUAH = 6;
    public static $SABTU = 7;

    public static function getString($type){
        //$type = intval($type."");
        switch($type){
            case self::$SABTU  : return "al-Sabtu";
            case self::$ISNAYNI  : return "al-Isnayni";
            case self::$TSULAATSA  : return "al-Tsulaatsa";
            case self::$ARBIA  : return "al-Arbi'a";
            case self::$KHAMIS  : return "al-Khamis";
            case self::$JUMUAH  : return "al-JUMU'AH";
            default : return "al-AHAD";
        }
    }
}