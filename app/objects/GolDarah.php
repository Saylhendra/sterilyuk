<?php
/**
 * Created by PhpStorm.
 * User: bitsolution
 * Date: 4/9/2015
 * Time: 4:41 PM
 */

class GolDarah
{
	//column type news
    public static $A = 1;
    public static $B = 2;
    public static $AB = 3;
    public static $O = 4;

    public static function getString($type){
        //$type = intval($type."");
        switch($type){
            case self::$A  : return "A";
            case self::$B  : return "B";
            case self::$AB  : return "AB";
            default : return "O";
        }
    }
}