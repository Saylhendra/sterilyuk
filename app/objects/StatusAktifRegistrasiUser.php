<?php
/**
 * Created by PhpStorm.
 * User: bitsolution
 * Date: 4/9/2015
 * Time: 4:41 PM
 */

class StatusAktifRegistrasiUser{

	//column type news
    public static $PENDING = 0;
    public static $AKTIF = 1;

    public static function getString($type){
        switch($type){
            case self::$AKTIF  : return "AKTIF";
            default : return "PENDING";
        }
    }
}