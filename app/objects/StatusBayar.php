<?php
/**
 * Created by PhpStorm.
 * User: bitsolution
 * Date: 4/9/2015
 * Time: 4:41 PM
 */

class StatusBayar{

	//column type news
    public static $PENDING = 0;
    public static $LUNAS = 1;

    public static function getString($type){
        switch($type){
            case self::$LUNAS  : return "LUNAS";
            default : return "PENDING";
        }
    }
}