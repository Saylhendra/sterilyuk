<?php
/**
 * Created by PhpStorm.
 * User: bitsolution
 * Date: 4/9/2015
 * Time: 4:41 PM
 */

class StatusHadir
{
	//column type news
    public static $TIDAK = 0;
    public static $HADIR = 1;
    public static function getStatus($type){
        //$type = intval($type."");
        switch($type){
            case self::$HADIR  : return "HADIR";
            default : return "TIDAK";
        }
    }
}