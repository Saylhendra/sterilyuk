<?php
/**
 * Created by PhpStorm.
 * User: bitsolution
 * Date: 4/9/2015
 * Time: 4:41 PM
 */

class TipeTim
{
	//column type news
    public static $HALAQOH_SQ = 100;
    public static $HALAQOH_184 = 101;
    public static $HALAQOH_106 = 102;
    public static $HALAQOH_223 = 103;
    public static $HALAQOH_PKP = 104;

    public static $TASK_FORCE = 2;

    public static function getString($type){
        //$type = intval($type."");
        switch($type){
            case self::$HALAQOH_SQ  : return "HALAQOH SQ";
            case self::$HALAQOH_184  : return "HALAQOH 184";
            case self::$HALAQOH_106  : return "HALAQOH 106";
            case self::$HALAQOH_223  : return "HALAQOH 223";
            case self::$HALAQOH_PKP  : return "HALAQOH PKP";
            default : return "TASK FORCE";
        }
    }
}