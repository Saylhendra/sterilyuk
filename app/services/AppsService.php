<?php

/**
 * Created by PhpStorm.
 * User: bitsolution
 * Date: 2/9/2015
 * Time: 2:46 PM
 */
class AppsService extends ServiceBase{

    public static function checkAppKey ( $app_key ){
        $client_app_key = AndroidApp::findFirst(array(
            "app_key =:app_key:",
            "bind" => array("app_key" => $app_key)
        ));
        if ( !$client_app_key ) {
            return false;
        } else {
            return true;
        }
    }

}