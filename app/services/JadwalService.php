<?php

class JadwalService extends ServiceBase{

    /*SAVING*/
	public static function saveJadwal($postData){
		$response = new ResponseObject();
		$response->message = "Failed: ";
		$response->isSuccess = false;

		try{
			$strJadwal = new StrJadwal();
            $strJadwal->id = generateUuidString();
            $strJadwal->assign($postData);

            $strJadwal->str_tgl = DateTimeLibrary::get_string_date($strJadwal->tgl_laksana);
            $strJadwal->str_bulan = DateTimeLibrary::get_string_month($strJadwal->tgl_laksana);;
            $strJadwal->str_thn = DateTimeLibrary::get_string_year($strJadwal->tgl_laksana);;
            $strJadwal->bts_tempo_bayar = 3; //batasan 3 hari default
            $strJadwal->sts_aktif = StatusAktifJadwal::$PENDING;

            $strJadwal->date_created = date("Y-m-d H:i:s");
            $strJadwal->date_update = date("Y-m-d H:i:s");
            $strJadwal->save();

			$response->isSuccess = true;
			$response->message = "Successfully";
            $response->data = $strJadwal;
		}catch(Exception $ex){
			$response->message .= $ex->getMessage();
		}

		return $response;
	}

    /*SAVING*/
    public static function saveJadwalDetil($postData, $idJadwal, $idJadwalShift){
        $response = new ResponseObject();
        $response->message = "Failed: ";
        $response->isSuccess = false;

        try{
            $strJadwalDetil = new StrJadwalDetil();
            $strJadwalDetil->id = generateUuidString();
            $strJadwalDetil->assign($postData);

            $strJadwalDetil->id_jadwal = $idJadwal;
            $strJadwalDetil->id_jadwal_shift = $idJadwalShift;

            $strJadwalDetil->tot_kuota = ($strJadwalDetil->betina_kuota + $strJadwalDetil->jantan_kuota);
            $strJadwalDetil->tot_sisa = ($strJadwalDetil->betina_sisa_kuota + $strJadwalDetil->betina_sisa_kuota);

            $strJadwalDetil->date_created = date("Y-m-d H:i:s");
            $strJadwalDetil->date_update = date("Y-m-d H:i:s");
            $strJadwalDetil->save();

            $response->isSuccess = true;
            $response->message = "Successfully";
            $response->data = $strJadwalDetil;
        }catch(Exception $ex){
            $response->message .= $ex->getMessage();
        }

        return $response;
    }

    /*LISTING*/
    public static function listJadwal(){
        $response = new ResponseObject();
        $response->message = "Failed: ";
        $response->isSuccess = false;

        try{
            $strJadwal = StrJadwal::find(array(
                "order" => "date_update DESC"
            ));

            $response->isSuccess = true;
            $response->message = "Query Successfully";
            $response->data = $strJadwal->toArray();
        }catch(Exception $ex){
            $response->message .= $ex->getMessage();
        }

        return $response;
    }

    public static function listJadwalAndDetail(){
        $response = new ResponseObject();
        $response->message = "Failed: ";
        $response->isSuccess = false;

        $result = array();

        try{
            $strJadwal = StrJadwal::find(array(
                "order" => "date_update DESC"
            ));
            foreach($strJadwal as $objJadwal){
                $result[] = array(
                    "id" => $objJadwal->id,
                    "id_jadwal_tipe" => $objJadwal->id_jadwal_tipe,
                    "lokasi" => $objJadwal->lokasi,
                    "alamat" => $objJadwal->alamat,
                    "tgl_laksana" => $objJadwal->tgl_laksana,
                    "str_tgl" => $objJadwal->str_tgl,
                    "str_bulan" => $objJadwal->str_bulan,
                    "str_thn" => $objJadwal->str_thn,
                    "sts_aktif" => $objJadwal->sts_aktif,
                    "bts_tempo_bayar" => $objJadwal->bts_tempo_bayar,
                    "notes" => $objJadwal->notes,
                    "date_created" => $objJadwal->date_created,
                    "date_update" => $objJadwal->date_update,
                    "photo_jadwal" => base_url("/tema1/images/office.jpg")
                );
                $jadwalDetil = StrJadwalDetil::findFirstByIdJadwal($objJadwal->id);
                if( $jadwalDetil ){
                    $result[] = array(
                        "id"=>$jadwalDetil->id,
                        "id_jadwal"=>$jadwalDetil->id_jadwal,
                        "id_jadwal_shift"=>$jadwalDetil->id_jadwal_shift,

                        "betina_kuota"=>$jadwalDetil->betina_kuota,
                        "betina_sisa_kuota"=>$jadwalDetil->betina_sisa_kuota,
                        "jantan_kuota"=>$jadwalDetil->jantan_kuota,
                        "jantan_sisa_kuota"=>$jadwalDetil->jantan_sisa_kuota,
                        "tot_kuota"=>$jadwalDetil->tot_kuota,
                        "tot_sisa"=>$jadwalDetil->tot_sisa,

                        "date_created"=>$jadwalDetil->date_created,
                        "date_update"=>$jadwalDetil->date_update
                    );
                }
            }

            $response->isSuccess = true;
            $response->message = "Query Successfully";
            $response->data = $result;
        }catch(Exception $ex){
            $response->message .= $ex->getMessage();
        }

        return $response;
    }

    /*LISTING*/
    public static function listJadwalDetil(){
        $response = new ResponseObject();
        $response->message = "Failed: ";
        $response->isSuccess = false;

        try{
            $strJadwal = StrJadwalDetil::find(array(
                "order" => "date_update DESC"
            ));

            $response->isSuccess = true;
            $response->message = "Query Successfully";
            $response->data = $strJadwal->toArray();
        }catch(Exception $ex){
            $response->message .= $ex->getMessage();
        }

        return $response;
    }

    /*DELETING*/
    public static function deleteJadwal($idJadwal = false){
        $response = new ResponseObject();
        $response->message = "Failed: ";
        $response->isSuccess = false;

        try{
            $strJadwal = \StrJadwal::findFirstById($idJadwal);
            if($strJadwal){
                $strJadwal->delete();
                $response->isSuccess = true;
                $response->message = "Delete Successfully";
                $response->data = $strJadwal->toArray();
            }
        }catch(Exception $ex){
            $response->message .= $ex->getMessage();
        }

        return $response;
    }

    /*DELETING*/
    public static function deleteJadwalDetil($idJadwal = false, $idJadwalShift){
        $response = new ResponseObject();
        $response->message = "Failed: ";
        $response->isSuccess = false;

        try{
            $conditions = "id_jadwal=:idJadwal: AND id_jadwal_shift=:idJadwalShift:";
            $bind = array("idJadwal"=>$idJadwal, "idJadwalShift"=>$idJadwalShift);
            $strJadwal = \StrJadwalDetil::findFirst(array("conditions"=>$conditions, "bind"=>$bind));
            if($strJadwal){
                $strJadwal->delete();
                $response->isSuccess = true;
                $response->message = "Delete Successfully";
                $response->data = $strJadwal->toArray();
            }
        }catch(Exception $ex){
            $response->message .= $ex->getMessage();
        }

        return $response;
    }
}