<?php

class MemberService extends ServiceBase{

	public static function loginAdmin( $username, $password ){
		$response = new ResponseObject();
		$response->isSuccess = TRUE;

		$user = \StrAccount::findFirst(array(
			"conditions"=>"username=:username: AND password=:password: ",
			"bind" => array(
				"username" => $username,
				"password" => md5($password)
			)
		));

		if ( !$user ) {
			$response->isSuccess = FALSE;
            $response->message = "Maaf, user tidak ditemukan.";
		}else{
			$response->message = "Terima Kasih.";
        }
		$response->data = $user;

		return $response;
	}

	public static function signinSuperAdminApp(){
        $response = new ResponseObject();
        $response->isSuccess = TRUE;
        $response->message = "Sign In Super Successfully";
        return $response;
	}

	public static function signinMemberApp($username, $password, $device_id, $os_type, $app_key){
        $response = new ResponseObject();
        $response->isSuccess = FALSE;
        $response->message = "Failed to Sign In, Please Check Username and Password.";
        $user_data = NULL;

        $userAccountProfile = array();

        $akun = \UserAccount::findFirst(array(
            "username=:username: AND password=:password: ",
            "bind" => array(
                "username" => $username,
                "password" => md5($password)
            )
        ));

		if( $akun ){
			$profile = \UserProfile::findFirstByIdAccount($akun->id);
            if($profile){
                $userAccountProfile = array(
                    "id_account" => $akun->id,
                    "username" => $akun->username,
                    "password" => $akun->password,
                    "id_role" => $akun->id_role,
                    "date_created" => $akun->date_created,
                    "date_updated" => $akun->date_updated,

                    "id_profile" => $profile->id,
                    "id_account" => $profile->id_account,
                    "full_name" => $profile->full_name,
                    "address" => $profile->address,
                    "phone" => $profile->phone,
                    "email" => $profile->email,
                    "path_original" => $profile->path_original,
                    "path_large" => $profile->path_large,
                    "path_medium" => $profile->path_medium,
                    "path_small" => $profile->path_small,
                    "path_thumbnails" => $profile->path_thumbnails,
                    "date_created" => $profile->date_created,
                    "date_update" => $profile->date_updated,
                );
            }

            $user_data = object_to_array($akun);
            $response->isSuccess = TRUE;
            $response->data = $userAccountProfile;
            $response->message = "Sign In Successfully";
        }

        return $response;
    }

    public static function signUpMember($username = false, $password = false){
        $response = new ResponseObject();
        $response->message = "Failed Sign Up: ";
        $response->isSuccess = FALSE;
        try{
            if( $username && $password ){
                $isUsernameExist = \UserAccount::findFirstByUsername($username);
                if( !$isUsernameExist ){
                    $userAccount = new \UserAccount();
                    $userAccount->id = generateUuidString();
                    $userAccount->username = $username;
                    $userAccount->password = md5($password);
                    $userAccount->id_role = "";

                    $userAccount->date_created = date('Y-m-d H:i:s');
                    $userAccount->date_updated = date('Y-m-d H:i:s');
                    $userAccount->save();

                    $userProfile = new \UserProfile();
                    $userProfile->id = generateUuidString();
                    $userProfile->id_account = $userAccount->id;
                    $userProfile->email = $username;
                    $userProfile->full_name = explode("@", $username)[0];
                    $userProfile->date_created = date('Y-m-d H:i:s');
                    $userProfile->date_updated = date('Y-m-d H:i:s');
                    $userProfile->save();

                    $response->message = "Daftar berhasil dilakukan.";
                    $response->isSuccess = TRUE;
                    $response->data = $userAccount;
                }else{
                    $response->message = "Maaf, email sudah ada. Silahkan daftar dengan email yang berbeda.";
                    $response->isSuccess = FALSE;
                    $response->data = $isUsernameExist;
                }
            }
        }catch(Exception $e){
            $response->message .= $e->getTraceAsString();
        }
        return $response;
    }

	public static function loginMemberApp( $username, $password , $device_id, $os_type, $app_key )
	{
		$response = new ResponseObject();
		$response->isSuccess = TRUE;
		$user_data = NULL;

		$user = PgPhl::findFirst(array(
			"username=:username: AND password=:password: ",
			"bind" => array(
				"username" => $username,
				"password" => md5($password)
			)
		));
		// check app_key
		$client_app_key = PgApp::findFirst(array(
			"app_key=:app_key:",
			"bind" => array(
				"app_key" => $app_key,
			)
		));

		if ( !$client_app_key ) {
			$response->isSuccess = FALSE;
			$response->message = T::message("login.message.invalidappkey");
		} else {
			if ( !$user ) {
				$response->isSuccess = FALSE;
				$response->message = T::message("login.message.error");
			} else {
				//else {
				$response->message = T::message("login.message.userloggedin");
				// get user primary photo

				//$historyLog = MemberService::createHistoryLogin($user->id,TypeLogin::$MOBILE_ANDROID);

				// create session token
				$user_token = generateRandomString(48);
				$session_token = new PgSessionToken();
				$session_token->id = generateUuidString();
				$session_token->id_app = $client_app_key->id;
				$session_token->id_member = $user->id;
				$session_token->session_key = $user_token;
				$session_token->device_id = $device_id;
				$session_token->os_type = $os_type;
				$session_token->created_time = date('Y-m-d H:i:s');
				//$session_token->session_log = $historyLog->data->id;
				if (!$session_token->save()) {
					$response->isSuccess = FALSE;
					$response->message = T::message("login.message.errorstatus");
				}
				$user_data = object_to_array($user);
				//$user_data = array_merge(object_to_array($user), $user_photo, $user_business, array("session_key" => $user_token));
			}
		}
		$response->data = $user_data;
		return $response;
	}

	public static function checkAppKey ( $app_key ){
		$client_app_key = PgApp::findFirst(array(
			"app_key =:app_key:",
			"bind" => array("app_key" => $app_key)
		));

		if ( !$client_app_key ) {
			return false;
		} else {
			return true;
		}
	}

	public static function checkSessionToken ( $session_key )
	{
		$client_session_key = PgSessionToken::findFirst(array(
			"session_key=:session_key:",
			"bind" => array(
				"session_key" => $session_key,
			)
		));

		if ( $client_session_key != false ) {
			return true;
		} else {
			return false;
		}
	}

	public static function getUserLoggedOnBySessionToken ( $session_key )
	{
		$client_session_key = PgSessionToken::findFirst(array(
			"session_key=:session_key:",
			"bind" => array(
				"session_key" => $session_key,
			)
		));

		if ( $client_session_key != false ) {
			return $client_session_key->id_member;
		} else {
			return null;
		}
	}

	public static function logoutMemberApp( $session_key, $id_member, $app_key ){

		$response = new ResponseObject();
		$response->isSuccess = TRUE;
		$user_data = NULL;

		// check app_key
		$client_app_key = PgApp::findFirst(array(
			"app_key=:app_key:",
			"bind" => array(
				"app_key" => $app_key,
			)
		));

		if ( !$client_app_key ) {
			$response->isSuccess = FALSE;
			$response->message = T::message("login.message.invalidappkey");
		} else {
			// check session key
			$session_app_key = PgSessionToken::findFirst(array(
				"session_key=:session_key: AND id_member=:id_member:",
				"bind" => array(
					"session_key" => $session_key,
					"id_member" => $id_member,
				)
			));

			if ( !$session_app_key ) {
				$response->isSuccess = FALSE;
				$response->message = T::message("login.message.invalidsessionkey");
			}

			if (is_found($session_app_key)) {
				if ($session_app_key->delete() === FALSE) {
					$response->isSuccess = FALSE;
					$response->message = T::message("login.message.errorlogout");
				} else {
					//MemberService::updateHistoryLogin($session_app_key->session_log);
					$response->isSuccess = TRUE;
					$response->message = T::message("login.message.successlogout");
				}
			} else {
				$response->isSuccess = FALSE;
				$response->message = T::message("login.message.erroralreadylogout");
			}
		}

		return $response;
	}

}