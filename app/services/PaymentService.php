<?php

class PaymentService extends ServiceBase{

    /*APPROVING*/
    public static function konfirmasiPayment($postData, $noReg){
        $response = new ResponseObject();
        $response->message = "Failed: ";
        $response->isSuccess = false;

        try{
            $strReg = StrRegistrasi::findFirstByNoReg($noReg);
            $strReg->assign($postData);

            $strReg->sts_bayar = StatusBayar::$LUNAS;
            $strReg->sts_aktif = StatusAktifRegistrasiUser::$AKTIF;

            $strReg->date_update = date("Y-m-d H:i:s");
            $strReg->update();

            $response->isSuccess = true;
            $response->message = "Successfully";
            $response->data = $strReg;
        }catch(Exception $ex){
            $response->message .= $ex->getMessage();
        }

        return $response;
    }

    /*SAVING*/
	public static function savePayment($postData, $noReg, $buktiBayarImg){
		$response = new ResponseObject();
		$response->message = "Failed: ";
		$response->isSuccess = false;

		try{
            $tblRegistrasi = StrRegistrasi::findFirstByNoReg($noReg);
            if( $tblRegistrasi ){
                $strPay = new StrPayment();
                $strPay->id = generateUuidString();
                $strPay->id_registrasi = $tblRegistrasi->id;
                $strPay->no_reg = $tblRegistrasi->no_reg;
                //$strPay->assign($postData);

                $strPay->date_created = date("Y-m-d H:i:s");
                $strPay->date_update = date("Y-m-d H:i:s");
                $strPay->save();

                PaymentService::savePaymentDetil($strPay, $strPay->id, $buktiBayarImg);

                $response->isSuccess = true;
                $response->message = "Successfully";
                $response->data = $strPay;
            }
		}catch(Exception $ex){
			$response->message .= $ex->getMessage();
		}

		return $response;
	}

    /*SAVING*/
    public static function savePaymentDetil($postData, $idPayment, $buktiBayarImg){
        $response = new ResponseObject();
        $response->message = "Failed: ";
        $response->isSuccess = false;

        try{
            $strPay = new StrPaymentDetil();
            $strPay->id = generateUuidString();
            $strPay->id_payment = $idPayment;
            //$strPay->assign($postData);
            /*LoggerLibrary::logDebug("LK;ASJDL;FJAS;LDFJA;LSDKJF");
            LoggerLibrary::logDebug($strPay);*/

            $doc = UploadLibrary::upload_picture_from_base64("parBuktiPembayaranImage", $buktiBayarImg);
            if ($doc['isSuccess']) {
                $strPay->path_original = $doc['path_original'];
                $strPay->path_large = $doc['path_large'];
                $strPay->path_medium = $doc['path_medium'];
                $strPay->path_small = $doc['path_small'];
                $strPay->path_thumbnails = $doc['path_thumbnails'];
            }

            $strPay->date_created = date("Y-m-d H:i:s");
            $strPay->date_update = date("Y-m-d H:i:s");
            $strPay->save();

            $response->isSuccess = true;
            $response->message = "Successfully";
            $response->data = $strPay;
        }catch(Exception $ex){
            $response->message .= $ex->getMessage();
        }

        return $response;
    }

    /*LISTING*/
    public static function listPayment(){
        $response = new ResponseObject();
        $response->message = "Failed: ";
        $response->isSuccess = false;

        try{
            $strJadwal = StrPayment::find(array(
                "order" => "date_update DESC"
            ));

            $response->isSuccess = true;
            $response->message = "Query Successfully";
            $response->data = $strJadwal->toArray();
        }catch(Exception $ex){
            $response->message .= $ex->getMessage();
        }

        return $response;
    }
    public static function listPaymentDetil(){
        $response = new ResponseObject();
        $response->message = "Failed: ";
        $response->isSuccess = false;

        try{
            $strJadwal = StrPaymentDetil::find(array(
                "order" => "date_update DESC"
            ));

            $response->isSuccess = true;
            $response->message = "Query Successfully";
            $response->data = $strJadwal->toArray();
        }catch(Exception $ex){
            $response->message .= $ex->getMessage();
        }

        return $response;
    }

    /*DELETING*/
    public static function deletePayment($noRegistrasi = false){
        $response = new ResponseObject();
        $response->message = "Failed: ";
        $response->isSuccess = false;

        try{
            $strReg = \StrRegistrasi::findFirstByNoReg($noRegistrasi);
            if($strReg){
                $strReg->delete();
                $response->isSuccess = true;
                $response->message = "Delete Successfully";
                $response->data = $strReg->toArray();
            }
        }catch(Exception $ex){
            $response->message .= $ex->getMessage();
        }

        return $response;
    }

}