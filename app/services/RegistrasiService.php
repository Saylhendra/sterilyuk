<?php

class RegistrasiService extends ServiceBase{

    /*APPROVING*/
    public static function approveRegistrasi($postData, $noReg){
        $response = new ResponseObject();
        $response->message = "Failed: ";
        $response->isSuccess = false;

        try{
            $strReg = StrRegistrasi::findFirstByNoReg($noReg);
            $strReg->assign($postData);

            $strReg->sts_bayar = StatusBayar::$LUNAS;
            $strReg->sts_aktif = StatusAktifRegistrasiUser::$AKTIF;

            $strReg->date_update = date("Y-m-d H:i:s");
            $strReg->update();

            $response->isSuccess = true;
            $response->message = "Successfully";
            $response->data = $strReg;
        }catch(Exception $ex){
            $response->message .= $ex->getMessage();
        }

        return $response;
    }

    /*SAVING*/
	public static function saveRegistrasi($postData){
		$response = new ResponseObject();
		$response->message = "Failed: ";
		$response->isSuccess = false;

		try{
			$strJadwal = new StrRegistrasi();
            $strJadwal->id = generateUuidString();
            $strJadwal->assign($postData);

            $strJadwal->no_reg = CommonLibrary::generateNoreg($postData);
            $strJadwal->qrcode = $strJadwal->no_reg;

            $strJadwal->sts_bayar = StatusBayar::$PENDING;
            $strJadwal->sts_aktif = StatusAktifRegistrasiUser::$PENDING;

            $strJadwal->date_created = date("Y-m-d H:i:s");
            $strJadwal->date_update = date("Y-m-d H:i:s");
            $strJadwal->save();

			$response->isSuccess = true;
			$response->message = "Successfully";
            $response->data = $strJadwal;
		}catch(Exception $ex){
			$response->message .= $ex->getMessage();
		}

		return $response;
	}

    /*LISTING*/
    public static function listRegistrasi(){
        $response = new ResponseObject();
        $response->message = "Failed: ";
        $response->isSuccess = false;

        try{
            $strJadwal = StrRegistrasi::find(array(
                "order" => "date_update DESC"
            ));

            $response->isSuccess = true;
            $response->message = "Query Successfully";
            $response->data = $strJadwal->toArray();
        }catch(Exception $ex){
            $response->message .= $ex->getMessage();
        }

        return $response;
    }

    /*DELETING*/
    public static function deleteRegistrasi($noRegistrasi = false){
        $response = new ResponseObject();
        $response->message = "Failed: ";
        $response->isSuccess = false;

        try{
            $strReg = \StrRegistrasi::findFirstByNoReg($noRegistrasi);
            if($strReg){
                $strReg->delete();
                $response->isSuccess = true;
                $response->message = "Delete Successfully";
                $response->data = $strReg->toArray();
            }
        }catch(Exception $ex){
            $response->message .= $ex->getMessage();
        }

        return $response;
    }

}