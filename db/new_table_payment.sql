drop table if exists str_payment;

/*==============================================================*/
/* Table: str_payment                                           */
/*==============================================================*/
create table str_payment
(
   id                   varchar(60) not null,
   id_registrasi        varchar(60),
   no_reg                varchar(100),
   date_created         datetime,
   date_update          datetime,
   primary key (id)
);

drop table if exists str_payment_detil;

/*==============================================================*/
/* Table: str_payment_detil                                     */
/*==============================================================*/
create table str_payment_detil
(
   id                   varchar(60) not null,
   id_payment           varchar(60),
   path_original        text,
   path_large           text,
   path_medium          text,
   path_small           text,
   path_thumbnails      text,
   tgl_transfer         date,
   nama_pengirim        varchar(100),
   norek_transfer       varchar(25),
   jumlah_transfer      double,
   date_created         datetime,
   date_update          datetime,
   primary key (id)
);
