/*==============================================================*/
/* Table: session_token                                         */
/*==============================================================*/
create table session_token
(
   id                   varchar(60) not null,
   id_app               varchar(60) not null,
   id_account           varchar(60) not null,
   session_key          varchar(128) not null,
   device_id            varchar(192) not null,
   os_type              varchar(50) not null,
   date_created         datetime not null,
   session_log          varchar(60),
   primary key (id)
);

/*==============================================================*/
/* Table: app_key                                               */
/*==============================================================*/
create table android_app
(
   id                   varchar(60) not null,
   app_name             varchar(60) not null,
   app_author           varchar(60) not null,
   app_key              varchar(192) not null,
   primary key (id)
);