drop table if exists str_registrasi;

/*==============================================================*/
/* Table: str_registrasi                                        */
/*==============================================================*/
create table str_registrasi
(
   id                   varchar(60) not null,
   id_acc               varchar(60),
   id_jadwal            varchar(60),
   no_reg               varchar(100),
   qrcode               text,
   sts_bayar            tinyint(4) comment '1=lunas
            0=kredit',
   sts_aktif            tinyint(4) comment '0 = diproses admin
            1 = diterima admin',
   date_created         datetime,
   date_update          datetime,
   primary key (id)
);