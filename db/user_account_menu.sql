/*==============================================================*/
/* Table: menu                                                  */
/*==============================================================*/
create table menu
(
   id                   varchar(60) not null,
   code                 varchar(9),
   code_parent          varchar(9),
   menu_name            varchar(100),
   path_url             text,
   path_icon            text,
   date_created         datetime,
   date_updated         datetime,
   primary key (id)
);

/*==============================================================*/
/* Table: role                                                  */
/*==============================================================*/
create table role
(
   id                   varchar(60) not null,
   role_name            varchar(100),
   date_created         datetime,
   date_updated         datetime,
   primary key (id)
);

/*==============================================================*/
/* Table: role_menu                                             */
/*==============================================================*/
create table role_menu
(
   id_role              varchar(60) not null,
   role_name            varchar(100),
   id_menu              varchar(60) not null,
   menu_name            varchar(100),
   date_created         datetime,
   date_updated         datetime,
   primary key (id_role, id_menu)
);

/*==============================================================*/
/* Table: user_account                                          */
/*==============================================================*/
create table user_account
(
   id                   varchar(60) not null,
   username             varchar(200),
   password             text,
   id_role              varchar(60),
   date_created         datetime,
   date_updated         datetime,
   primary key (id)
);

/*==============================================================*/
/* Table: user_profile                                          */
/*==============================================================*/
create table user_profile
(
   id                   varchar(60) not null,
   id_account           varchar(60),
   full_name            varchar(100),
   address              text,
   phone                varchar(20),
   email                varchar(150),
   path_original        text,
   path_large           text,
   path_medium          text,
   path_small           text,
   path_thumbnails      text,
   date_created         datetime,
   date_update          datetime,
   primary key (id)
);