<?php

error_reporting(E_ALL);
date_default_timezone_set("Asia/Jakarta");

set_error_handler(function ( $errno, $errstr, $errfile, $errline ) {
    /*
    if ( !(error_reporting() & $errno) ) {
        // This error code is not included in error_reporting
        return;
    }
    */
    throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
});

try {

    /**
     * Read the configuration
     */
    $GLOBALS[ 'CONFIG_APP' ] = $config = include __DIR__ . "/../app/config/config.php";
    $GLOBALS[ 'PUBLIC_DIR' ] = __DIR__;

    /**
     * Read auto-loader
     */
    include __DIR__ . "/../app/config/loader.php";

    /**
     * Read services
     */
    include __DIR__ . "/../app/config/services.php";


    include __DIR__ . "/../app/config/functions.php";
    /**
     * Handle the request
     */
    $application = new \Phalcon\Mvc\Application($di);


    echo $application->handle()->getContent();


} catch (\Exception $e) {
    ob_start();
    //echo $e->getMessage();
    $data = array("isSuccess"=>false,"message"=>"ERROR : ".$e->getMessage());
    echo json_encode($data);
}


include __DIR__ . "/../app/config/micro.php";

